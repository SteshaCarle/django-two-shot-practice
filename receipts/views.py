from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm


# Create your views here.
@login_required
def receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts
        }
    return render(request, "receipts/receipt.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create_receipt.html", context)

@login_required
def category_list(request):
    receipts_in_category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category": receipts_in_category
    }
    return render(request, "receipts/category_list.html", context)

@login_required
def account_list(request):
    receipts_in_account = Account.objects.filter(owner=request.user)
    context = {
        "account": receipts_in_account
    }
    return render(request, 'receipts/account_list.html', context)
